class FunctionInPage {
  constructor(page) {
    this.page = page;
  }

  async navigate(url) {
    await this.page.goto(url);
  }

  async scrollDownToBottom(
    distance = 100,
    timePerScroll = 100,
    timeLimits = 600000,
    aPartOfScreen = 1
  ) {
    await this.page.evaluate(
      async ([distance, timePerScroll, timeLimits, aPartOfScreen]) => {
        await new Promise((resolve) => {
          let totalHeight = 0;
          let timer = setInterval(() => {
            timeLimits -= timePerScroll;
            let scrollHeight = document.body.scrollHeight * aPartOfScreen;
            window.scrollBy(0, distance);
            totalHeight += distance;

            if (totalHeight >= scrollHeight || timeLimits <= 0) {
              clearInterval(timer);
              resolve();
            }
          }, timePerScroll);
        });
      },
      [distance, timePerScroll, timeLimits, aPartOfScreen]
    );
  }

  async scrollUpToTop(
    distance = 100,
    timePerScroll = 100,
    timeLimits = 600000,
    aPartOfScreen = 0
  ) {
    await this.page.evaluate(
      async ([distance, timePerScroll, timeLimits, aPartOfScreen]) => {
        await new Promise((resolve) => {
          let totalHeight = document.body.scrollHeight;
          let timer = setInterval(() => {
            let scrollHeight = document.body.scrollHeight * aPartOfScreen;
            timeLimits -= timePerScroll;
            window.scrollBy(0, -distance);
            totalHeight -= distance;

            if (totalHeight <= scrollHeight || timeLimits <= 0) {
              clearInterval(timer);
              resolve();
            }
          }, timePerScroll);
        });
      },
      [distance, timePerScroll, timeLimits, aPartOfScreen]
    );
  }

  async delayToRead(time) {
    await this.page.waitForTimeout(time);
  }
}

module.exports = { FunctionInPage };
