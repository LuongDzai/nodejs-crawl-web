const { firefox } = require("playwright");
const { FunctionInPage } = require("./functionInPage");

(async () => {
  const browser = await firefox.launch({ headless: false });
  const page = await browser.newPage();

  const vnPage = new FunctionInPage(page);
  await vnPage.navigate("https://vnexpress.net/");
  await vnPage.scrollDownToBottom(100, 500, undefined, 0.8);
  await vnPage.delayToRead(2000);
  await vnPage.scrollUpToTop(100, 100, undefined, 0.3);
  await browser.close();
})();
