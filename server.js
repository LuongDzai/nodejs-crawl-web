const http = require("http");
const { chromium } = require("playwright");

// get data with post method
const getPostData = (req) => {
  return new Promise((resolve, reject) => {
    try {
      let body = "";
      req.on("data", (chunk) => {
        body += chunk.toString();
      });

      req.on("end", () => {
        resolve(body);
      });
    } catch (error) {
      reject(error);
    }
  });
};

// Crawl Web
const getHtmlAndDataWeb = async (req, res) => {
  try {
    // Get url from body req with post
    // const body = await getPostData(req);
    const url = req.url.substr(6);
    const checkURL = RegExp(
      "(https?://(?:www.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|www.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9].[^s]{2,}|https?://(?:www.|(?!www))[a-zA-Z0-9]+.[^s]{2,}|www.[a-zA-Z0-9]+.[^s]{2,})"
    );

    // Handle url to get html web
    if (!checkURL.test(url)) {
      res.writeHead(400, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: "Data not good!" }));
    } else {
      const browser = await chromium.launch();
      const context = await browser.newContext();
      const page = await context.newPage();
      await page.goto(url);
      let html = await page.content();
      await browser.close();

      // Return data
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(html);
      res.end();
    }
  } catch (error) {
    res.writeHead(400, { "Content-Type": "application/json" });
    res.end(JSON.stringify({ message: "Data not good!" }));
  }
};

// Create Server
const server = http.createServer((req, res) => {
  // Check URL start with right param
  const checkURL = /^\/\?url=/;
  if (checkURL.test(req.url) && req.method === "GET") {
    getHtmlAndDataWeb(req, res);
  } else {
    res.writeHead(404, { "Content-Type": "application/json" });
    res.end(JSON.stringify({ message: "Route not found!" }));
  }
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => {
  console.log(`Running on port ${PORT}`);
});
